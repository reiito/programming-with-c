COSC1254 Programming for C++ - Assignment 1
Author: Rei Ito (s3607050)

Submitting (zipped):
 - src (*.cpp, *.h)
 - obj (*.o)
 - data (*.txt, *.dat, *.dict)
 - Makefile
 - README.md
Late: 27/08/17

Notes:
I have not completed all functionality and requirements, incomplete:
 - bst search compare and store
 - edit distance (fuzzy)
